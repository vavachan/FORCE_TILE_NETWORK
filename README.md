## FORCE TILE NETWORK 
A jammed configurations by definition is such that all the particles in the system 
( except for rattlers ) are under force-balance or the sum of all 
forces acting on the particle add upto zero. There we can arrange the forces acting  
on the particle such that they form a closed polygon. In systems where we can only have 
normal forces these polygons are always convex. 

This code calculates the force-tile-network given the forces in a force-balanced 
jammed configuration.

To run the code use 
`python force_tile.py [edge_list] [N] [tilt]`

where `[edge_list]` is a file which has the following format:
```
id1	id2	dr	f_x	f_y	|f|
	.
	.
	.
```

and N is the number of particles in the system and [tilt] is  the tilt in the system 
which does not play a role in calculating the force-tile-network ( it is passed to format the output).
